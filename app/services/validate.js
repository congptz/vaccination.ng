function isVietnamesePhoneNumber(paramNumber) {
    return /(03|05|07|08|09|01[2|6|8|9])+([0-9]{8})\b/.test(paramNumber);
}

function isEmail(paramEmail) {
  return  /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i.test(paramEmail);
}

module.exports = {
    isVietnamesePhoneNumber,
    isEmail,
}