//import thư viện mongoose
const mongoose = require('mongoose');

//import user model
const userModel = require('../models/user.model');

//import validate.js
const validation = require('../services/validate.js');

//viết api có async await
const createUser = async (req, res) => {
    // B1: Chuẩn bị dữ liệu
    const {
        fullName,
        phone,
        status,
    } = req.body;

    // B2: Validate dữ liệu
    if (!fullName) {
        return res.status(400).json({
            status: "Bad request",
            message: "fullName is required!"
        })
    }
    if (!phone) {
        return res.status(400).json({
            status: "Bad request",
            message: "phone is required!"
        })
    }
    if (!validation.isVietnamesePhoneNumber(phone)) {
        return res.status(400).json({
            status: "Bad request",
            message: "phone is invalid!"
        })
    }

    // B3: Thao tác với CSDL
    var newUser = {
        _id: new mongoose.Types.ObjectId(),
        fullName,
        phone,
        status,
    }

    try {
        const createdUser = await userModel.create(newUser);

        return res.status(201).json({
            status: "Create user successfully",
            data: createdUser
        })
    } catch (error) {
        if (error.name == "MongoServerError" && error.code == 11000) {
            return res.status(400).json({
                status: "Bad request",
                message: "duplicate phone number",
            })
        } else {
            return res.status(500).json({
                status: "Internal server error",
                message: error.message,
            })
        }
    }
}

const getAllUser = async (req, res) => {
    //B1: thu thập dữ liệu
    //B2: kiểm tra
    //B3: thực thi model
    try {
        const userList = await userModel.find();

        if (userList && userList.length > 0) {
            return res.status(200).json({
                status: "Get all users sucessfully",
                data: userList
            })
        } else {
            return res.status(404).json({
                status: "Not found any user"
            })
        }
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }
}

const getUserById = async (req, res) => {
    //B1: thu thập dữ liệu
    var userId = req.params.userId;

    //B2: kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(userId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "userId is invalid!"
        })
    }
    //B3: thực thi model
    try {
        const userInfo = await userModel.findById(userId);

        if (userInfo) {
            return res.status(200).json({
                status: "Get user by id sucessfully",
                data: userInfo
            })
        } else {
            return res.status(404).json({
                status: "Not found any user"
            })
        }
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }
}

const updateUser = async (req, res) => {
    //B1: thu thập dữ liệu
    var userId = req.params.userId;

    const {
        fullName,
        phone,
        status,
    } = req.body;

    //B2: kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(userId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "userId is invalid!"
        })
    }
    if (!fullName) {
        return res.status(400).json({
            status: "Bad request",
            message: "fullName is required!"
        })
    }
    if (!phone) {
        return res.status(400).json({
            status: "Bad request",
            message: "phone is required!"
        })
    }
    if (!validation.isVietnamesePhoneNumber(phone)) {
        return res.status(400).json({
            status: "Bad request",
            message: "phone is invalid!"
        })
    }

    //B3: thực thi model
    try {
        let updateUser = {
            fullName,
            phone,
            status,
        }

        const updatedUser = await userModel.findByIdAndUpdate(userId, updateUser);
        const userAfterUpdate = await userModel.findById(userId);

        if (updatedUser) {
            return res.status(200).json({
                status: "Update user sucessfully",
                data: userAfterUpdate
            })
        } else {
            return res.status(404).json({
                status: "Not found any user"
            })
        }
    } catch (error) {
        if (error.name == "MongoServerError" && error.code == 11000) {
            return res.status(400).json({
                status: "Bad request",
                message: "duplicate phone number",
            })
        } else {
            return res.status(500).json({
                status: "Internal server error",
                message: error.message,
            })
        }
    }
}

const deleteUser = async (req, res) => {
    //B1: Thu thập dữ liệu
    var userId = req.params.userId;

    //B2: kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(userId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "userId is invalid!"
        })
    }

    //B3: thực thi model
    try {
        const deletedUser = await userModel.findByIdAndDelete(userId);

        if (deletedUser) {
            return res.status(200).json({
                status: "Delete user sucessfully",
                data: deletedUser
            })
        } else {
            return res.status(404).json({
                status: "Not found any user"
            })
        }
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }
}

module.exports = {
    createUser,
    getAllUser,
    getUserById,
    updateUser,
    deleteUser,
}