//import thư viện mongoose
const mongoose = require('mongoose');

//import contact model
const contactModel = require('../models/contact.model');

//import validate.js
const validation = require('../services/validate.js');

//viết api có async await
const createContact = async (req, res) => {
    // B1: Chuẩn bị dữ liệu
    const {
        email,
    } = req.body;

    // B2: Validate dữ liệu
    if (!email) {
        return res.status(400).json({
            status: "Bad request",
            message: "email is required!"
        })
    }
    if (!validation.isEmail(email)) {
        return res.status(400).json({
            status: "Bad request",
            message: "email is invalid!"
        })
    }

    // B3: Thao tác với CSDL
    var newContact = {
        _id: new mongoose.Types.ObjectId(),
        email,
    }

    try {
        const createdContact = await contactModel.create(newContact);

        return res.status(201).json({
            status: "Create contact successfully",
            data: createdContact
        })
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }
}

const getAllContact = async (req, res) => {
    //B1: thu thập dữ liệu
    //B2: kiểm tra
    //B3: thực thi model
    try {
        const contactList = await contactModel.find();

        if (contactList && contactList.length > 0) {
            return res.status(200).json({
                status: "Get all contacts sucessfully",
                data: contactList
            })
        } else {
            return res.status(404).json({
                status: "Not found any contact"
            })
        }
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }
}

const getContactById = async (req, res) => {
    //B1: thu thập dữ liệu
    var contactId = req.params.contactId;

    //B2: kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(contactId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "contactId is invalid!"
        })
    }
    //B3: thực thi model
    try {
        const contactInfo = await contactModel.findById(contactId);

        if (contactInfo) {
            return res.status(200).json({
                status: "Get contact by id sucessfully",
                data: contactInfo
            })
        } else {
            return res.status(404).json({
                status: "Not found any contact"
            })
        }
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }
}

const updateContact = async (req, res) => {
    //B1: thu thập dữ liệu
    var contactId = req.params.contactId;

    const {
        email,
    } = req.body;

    //B2: kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(contactId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "contactId is invalid!"
        })
    }
    if (!email) {
        return res.status(400).json({
            status: "Bad request",
            message: "email is required!"
        })
    }
    if (!validation.isEmail(email)) {
        return res.status(400).json({
            status: "Bad request",
            message: "email is invalid!"
        })
    }

    //B3: thực thi model
    try {
        let updateContact = {
            email,
        }

        const updatedContact = await contactModel.findByIdAndUpdate(contactId, updateContact);
        const contactAfterUpdate = await contactModel.findById(contactId);

        if (updatedContact) {
            return res.status(200).json({
                status: "Update contact sucessfully",
                data: contactAfterUpdate
            })
        } else {
            return res.status(404).json({
                status: "Not found any contact"
            })
        }
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }
}

const deleteContact = async (req, res) => {
    //B1: Thu thập dữ liệu
    var contactId = req.params.contactId;

    //B2: kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(contactId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "contactId is invalid!"
        })
    }

    //B3: thực thi model
    try {
        const deletedContact = await contactModel.findByIdAndDelete(contactId);

        if (deletedContact) {
            return res.status(200).json({
                status: "Delete contact sucessfully",
                data: deletedContact
            })
        } else {
            return res.status(404).json({
                status: "Not found any contact"
            })
        }
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }
}

module.exports = {
    createContact,
    getAllContact,
    getContactById,
    updateContact,
    deleteContact,
}