const express = require("express");

const contactController = require("../controllers/contact.controller");

const router = express.Router();

router.use((req, res, next) => {
    console.log("Request method: ", req.method);

    next();
});

router.get("/", contactController.getAllContact)

router.post("/", contactController.createContact);

router.get("/:contactId", contactController.getContactById);

router.put("/:contactId", contactController.updateContact);

router.delete("/:contactId", contactController.deleteContact);

module.exports = router;