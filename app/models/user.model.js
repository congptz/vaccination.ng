//b1: khai báo thư viện mongo
const mongoose = require("mongoose")

//b2: khai báo class Schema
const Schema = mongoose.Schema

//b3: khởi tạo Shema với các thuộc tính của collection
const userSchema = new Schema({
    _id: mongoose.Types.ObjectId, //có thể khai báo hoặc không, vì mongoose thường tự sinh
    fullName: {
        type: String,
        required: true,
    },
    phone: {
        type: String,
        required: true,
        unique: true,
    },
    status: {
        type: String,
        default: "Level 0",
    }
},
{
    timestamps: true,
});

//b4:biên dịch shema thành model
module.exports = mongoose.model("user", userSchema);