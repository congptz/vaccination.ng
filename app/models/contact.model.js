//b1: khai báo thư viện mongo
const mongoose = require("mongoose")

//b2: khai báo class Schema
const Schema = mongoose.Schema

//b3: khởi tạo Shema với các thuộc tính của collection
const contactSchema = new Schema({
    _id: mongoose.Types.ObjectId, //có thể khai báo hoặc không, vì mongoose thường tự sinh
    email: {
        type: String,
        required: true,
    },
},
{
    timestamps: true,
});

//b4:biên dịch shema thành model
module.exports = mongoose.model("contact", contactSchema);