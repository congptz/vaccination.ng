const express = require("express"); // Tương tự : import express from "express";
const serverless = require("serverless-http");
const path = require("path");

// Khởi tạo Express App
const app = express();

// Khai báo ứng dụng sẽ chạy trên cổng 8000
const port = 8000;

//Khai báo mongoose
var mongoose = require('mongoose');

// Khai báo ứng dụng đọc được body raw json (Build in middleware)
app.use(express.json());

//kết nối mongodb
mongoose.connect("mongodb://127.0.0.1:27017/Vaccination-mid-module")
    .then(() => console.log("Connected to Mongo Successfully"))
    .catch( (error) =>  {
        console.log(error);
    });

//Middleware in ra console thời gian hiện tại mỗi khi API gọi
app.use((req, res, next) => {
    var today = new Date();

    console.log("Current time: ", today);   

    next();
});

//Middleware in ra console request method mỗi khi API gọi
app.use((req, res, next) => {
    console.log("Request URL: ", req.url);

    next();
});

//load các thành phần tĩnh như ảnh, css
app.use(express.static(__dirname + "/app/views"))

// Khai báo APi dạng Get "/" sẽ chạy vào đây
app.get("/", (req, res) => {
    console.log(__dirname);

    res.sendFile(path.join(__dirname + "/app/views/index.html"))

})

app.get("/admin/users", (req, res) => {
    console.log(__dirname);

    res.sendFile(path.join(__dirname + "/app/views/admin/users.html"))
})

app.get("/admin/contacts", (req, res) => {
    console.log(__dirname);

    res.sendFile(path.join(__dirname + "/app/views/admin/contacts.html"))
})

//khai báo routers
const userRouter = require("./app/routers/user.router");
const contactRouter = require("./app/routers/contact.router");

app.use("/api/v1/users", userRouter);
app.use("/api/v1/contacts", contactRouter);

app.listen(port, () => {
    console.log(`App Listening on port ${port}`);
})

module.exports.handler = serverless(app);